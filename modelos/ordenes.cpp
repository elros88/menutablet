#include "ordenes.h"


Ordenes::Ordenes(QObject *parent)
    : QAbstractListModel(parent)
{

}

QJsonArray Ordenes::getJsonOrdenes() const
{
    return jsonOrdenes;
}

void Ordenes::setJsonOrdenes(const QJsonArray &value)
{
    jsonOrdenes = value;
}


void Ordenes::iniciar()
{
    for(int i=0; i<jsonOrdenes.size() ; i++)
    {
        QJsonObject jsonOrden = jsonOrdenes[i].toObject();
        agregarOrden(jsonOrden, i);
    }

}

void Ordenes :: agregarOrden(QJsonObject jsonOrden, int posicion)
{
    beginInsertRows(QModelIndex(), posicion, posicion);
    orden.append(new Orden);
    orden[posicion]->setDescripcion(jsonOrden.value("descript").toString());
    orden[posicion]->setCodigo(jsonOrden.value("ordercat").toString());
    endInsertRows();


}

QHash<int, QByteArray> Ordenes::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[codigo] = "codigo";
    roles[posicion] = "posicion";
    roles[descripcion] = "descripcion";
    roles[listaProductos] = "listaProductos";
    return roles;
}

QVariant Ordenes::data(const QModelIndex & index, int role) const {

    if (index.row() < 0 || index.row() >= orden.count())
    {
        return QVariant();
    }

    switch (role)
    {
        case codigo:
            return orden[index.row()]->getCodigo();
        case posicion:
            return orden[index.row()]->getPosicion();
        case descripcion:
            return orden[index.row()]->getDescripcion();
        case listaProductos:
            return QVariant::fromValue<QObject *>(orden[index.row()]->getProductos());
        default:
            break;
    }

    return QVariant();
}

int Ordenes::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent);
    return orden.count();
}

QString Ordenes::getCodigo(int indice)
{
    return orden[indice]->getCodigo();
}
