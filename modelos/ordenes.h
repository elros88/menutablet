#ifndef ORDENES_H
#define ORDENES_H

#include <QObject>
#include <QAbstractListModel>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDebug>
#include "orden.h"

class Ordenes : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QJsonArray jsonOrdenes READ getJsonOrdenes WRITE setJsonOrdenes)

private:
    QList<Orden*> orden;
    QJsonArray jsonOrdenes;

public:
    Ordenes(QObject *parent = 0);

    enum roles{
        codigo = Qt::UserRole + 1,
        posicion,
        descripcion,
        listaProductos
    };


    Q_INVOKABLE void iniciar ();

    QHash<int, QByteArray> roleNames() const;
    QVariant data(const QModelIndex &index, int role) const;
    int rowCount(const QModelIndex &parent) const;
    QJsonArray getJsonOrdenes() const;
    void setJsonOrdenes(const QJsonArray &value);

    Q_INVOKABLE void agregarOrden(QJsonObject jsonOrden, int posicion);
    Q_INVOKABLE QString getCodigo(int indice);
signals:

public slots:
};

#endif // ORDENES_H
