#include "pedido.h"

QString Pedido::getNumeroValor1() const
{
    return numeroValor1;
}

void Pedido::setNumeroValor1(const QString &value)
{
    numeroValor1 = value;
}

QString Pedido::getNumeroValor2() const
{
    return numeroValor2;
}

void Pedido::setNumeroValor2(const QString &value)
{
    numeroValor2 = value;
}

QString Pedido::getNumeroValor3() const
{
    return numeroValor3;
}

void Pedido::setNumeroValor3(const QString &value)
{
    numeroValor3 = value;
}

QString Pedido::getNumeroValor4() const
{
    return numeroValor4;
}

void Pedido::setNumeroValor4(const QString &value)
{
    numeroValor4 = value;
}

Pedido::Pedido(QObject *parent) : QObject(parent)
{

}

QString Pedido::getNumeroPregunta4() const
{
    return numeroPregunta4;
}

void Pedido::setNumeroPregunta4(const QString &value)
{
    numeroPregunta4 = value;
}

QString Pedido::getNumeroPregunta3() const
{
    return numeroPregunta3;
}

void Pedido::setNumeroPregunta3(const QString &value)
{
    numeroPregunta3 = value;
}

QString Pedido::getNumeroPregunta2() const
{
    return numeroPregunta2;
}

void Pedido::setNumeroPregunta2(const QString &value)
{
    numeroPregunta2 = value;
}

QString Pedido::getNumeroPregunta1() const
{
    return numeroPregunta1;
}

void Pedido::setNumeroPregunta1(const QString &value)
{
    numeroPregunta1 = value;
}

QString Pedido::getPrecioProducto() const
{
    return precioProducto;
}

void Pedido::setPrecioProducto(const QString &value)
{
    precioProducto = value;
}

QString Pedido::getNombreProducto() const
{
    return nombreProducto;
}

void Pedido::setNombreProducto(const QString &value)
{
    nombreProducto = value;
}

QString Pedido::getNumeroProducto() const
{
    return numeroProducto;
}

void Pedido::setNumeroProducto(const QString &value)
{
    numeroProducto = value;
}

QString Pedido::getNumeroOrden() const
{
    return numeroOrden;
}

void Pedido::setNumeroOrden(const QString &value)
{
    numeroOrden = value;
}

