#include "preguntas.h"

Preguntas::Preguntas(QObject *parent) : QObject(parent)
{
    for(int i = 0; i<4; i++)
    {
        pregunta.append(new Pregunta(i, this));
    }
}


QList<Pregunta*> Preguntas::getPregunta() const
{
    return pregunta;
}

void Preguntas::setPregunta(const QList<Pregunta*> &value)
{
    pregunta = value;
}

QJsonArray Preguntas::getJson() const
{
    return json;
}

void Preguntas::setJson(const QJsonArray &value)
{
    json = value;
}

void Preguntas::iniciar(int posicion)
{
    pregunta[posicion]->guardarValores(json);
}

QVariant Preguntas::getModelo(int posicion)
{
    return QVariant::fromValue<QObject *>(pregunta[posicion]->getValores());
}

void Preguntas::elegirValor(int posicionValor, int posicionPregunta)
{
    pregunta[posicionPregunta]->elegirValor(posicionValor);
}

bool Preguntas::verificarVisible(int posicion)
{
    return pregunta[posicion]->getVisible();
}

QString Preguntas::leerCodigoValor(int posicionValor, int posicionPregunta)
{
    return pregunta[posicionPregunta]->leerCodigo(posicionValor);
}
