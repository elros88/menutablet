#ifndef PEDIDOS_H
#define PEDIDOS_H

#include <QObject>
#include <QAbstractListModel>
#include "pedido.h"

class Pedidos : public QAbstractListModel
{
    Q_PROPERTY(bool llevar READ getLlevar WRITE setLlevar NOTIFY llevarCambio)

    QList <Pedido*> pedido;
    bool llevar;

    Q_OBJECT
public:
    explicit Pedidos(QObject *parent = 0);

    enum roles{
        numeroOrden = Qt::UserRole + 1,
        numeroProducto,
        nombreProducto,
        precioProducto,
        numeroPregunta1,
        numeroPregunta2,
        numeroPregunta3,
        numeroPregunta4,
        numeroValor1,
        numeroValor2,
        numeroValor3,
        numeroValor4,
    };


    QHash<int, QByteArray> roleNames() const;
    QVariant data(const QModelIndex &index, int role) const;
    int rowCount(const QModelIndex &parent) const;

    Q_INVOKABLE void agregarPedido(QString numeroOrden,
                                   QString numeroProducto,
                                   QString nombreProducto,
                                   QString precioProducto,
                                   QString numeroPregunta1,
                                   QString numeroPregunta2,
                                   QString numeroPregunta3,
                                   QString numeroPregunta4,
                                   QString numeroValor1,
                                   QString numeroValor2,
                                   QString numeroValor3,
                                   QString numeroValor4);

    bool getLlevar() const;
    void setLlevar(bool value);

signals:
    void llevarCambio();
public slots:
};

#endif // PEDIDOS_H
