#include "direcciones.h"

Direcciones::Direcciones (QObject *parent) : QObject(parent)
{
    setErrorLectura(false);
    base = "";

    QFile archivoIP("recursos/servidor");
    if(!archivoIP.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        setErrorLectura(true);
    }
    else
    {
        QTextStream entrada(&archivoIP);
        while(!entrada.atEnd())
        {
           base = entrada.readLine();
           qDebug() << "La ip del Servidor es " << base;
        }
    }

    ordenes = base + "paginas";
    productos = base + "productos/";
    detalles = base + "detalle";
    preguntas = base + "pregunta";
    login = base + "login";
}

QString Direcciones::getBase() const
{
    return base;
}

QString Direcciones::getOrdenes() const
{
    return ordenes;
}

QString Direcciones::getProductos() const
{
    return productos;
}

QString Direcciones::getDetalles() const
{
    return detalles;
}

QString Direcciones::getPreguntas() const
{
    return preguntas;
}

QString Direcciones::getLogin() const
{
    return login;
}

void Direcciones::setBase(const QString &value)
{
    base = value;
    qDebug()<<value;

    QFile archivoIP("recursos/servidor");

    if(!archivoIP.open(QIODevice::WriteOnly | QIODevice::Text))
    {
       setErrorLectura(true);
       return;
    }

    QTextStream salida(&archivoIP);
    salida<<base<<"\n";
}

bool Direcciones::getErrorLectura() const
{
    return errorLectura;
}

void Direcciones::setErrorLectura(bool value)
{
    errorLectura = value;
    emit errorLecturaCambio();
}
