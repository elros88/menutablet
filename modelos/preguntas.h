#ifndef PREGUNTAS_H
#define PREGUNTAS_H

#include <QObject>
#include <QAbstractListModel>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDebug>

#include "modelos/pregunta.h"

class Preguntas : public QObject
{
    Q_PROPERTY(QJsonArray json READ getJson WRITE setJson)
    Q_OBJECT

    QList<Pregunta*> pregunta;

    QJsonArray json;

public:
    explicit Preguntas(QObject *parent = 0);

    QList<Pregunta*> getPregunta() const;
    void setPregunta(const QList<Pregunta*> &value);
    QJsonArray getJson() const;
    void setJson(const QJsonArray &value);

    Q_INVOKABLE void iniciar(int posicion);
    Q_INVOKABLE QVariant getModelo(int posicion);
    Q_INVOKABLE void elegirValor(int posicionValor, int posicionPregunta);
    Q_INVOKABLE bool verificarVisible(int posicion);
    Q_INVOKABLE QString leerCodigoValor(int posicionValor, int posicionPregunta);
};
#endif // PREGUNTAS_H
