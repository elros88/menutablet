#include "valores.h"

Valores::Valores(QObject *parent) : QAbstractListModel(parent)
{

}

int Valores::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return valor.count();
}

QVariant Valores::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= valor.count())
    {
        return QVariant();
    }

    switch (role)
    {
        case codigo:
            return valor[index.row()]->getCodigo();
        case nombre:
            return valor[index.row()]->getNombre();
        case elegido:
            return valor[index.row()]->getElegido();
        default:
            break;

    }

    return QVariant();
}

QHash<int, QByteArray> Valores::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[codigo] = "codigo";
    roles[nombre] = "nombre";
    roles[elegido] = "elegido";
    return roles;
}

void Valores::iniciar(QJsonArray jsonValores)
{
    qDebug()<<"Iniciando Valores";

    qDebug()<<jsonValores.size();

    for (int i = 0; i<jsonValores.size(); i++)
    {
        QJsonObject jsonValor = jsonValores[i].toObject();
        qDebug()<<"Guardando JSON";
        agregarValor(i, jsonValor);
    }

}

void Valores::agregarValor(int posicion, QJsonObject jsonValor)
{
    qDebug()<<"Agregando el valor numero "<<posicion;

    beginInsertRows(QModelIndex(), posicion, posicion);

    valor.append(new Valor);

    qDebug()<<valor.size();

    valor[posicion]->setCodigo(jsonValor.value("optionindex").toString());
    valor[posicion]->setNombre(jsonValor.value("descript").toString());
    valor[posicion]->setElegido(false);

    endInsertRows();
}

void Valores::elegir(int posicion)
{
    qDebug()<<"buscar "<<posicion;
    qDebug()<<" entre "<< valor.size();
    for(int i = 0; i<valor.size(); i++)
    {
        qDebug()<<i;
        if(i == posicion)
        {
            qDebug()<<valor[i]->getNombre()<<" Elegido";
            valor[i]->setElegido(true);
        }
        else
        {
            qDebug()<<valor[i]->getNombre()<<" No Elegido";
            valor[i]->setElegido(false);
        }

    }
}

QString Valores::leerCodigo(int posicion)
{
    return valor[posicion]->getCodigo();
}
