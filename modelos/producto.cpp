#include "producto.h"

Producto::Producto(QObject *parent) : QObject(parent)
{
    
}

float Producto::getPrecio() const
{
    return precio;
}

void Producto::setPrecio(float value)
{
    precio = value;
}

QString Producto::getDescripcion() const
{
    return descripcion;
}

void Producto::setDescripcion(const QString &value)
{
    descripcion = value;
}

QString Producto::getCodigo() const
{
    return codigo;
}

void Producto::setCodigo(const QString &value)
{
    codigo = value;
}

QString Producto::getNombre() const
{
    return nombre;
}

void Producto::setNombre(const QString &value)
{
    nombre = value;
}

int Producto::getPosicion() const
{
    return posicion;
}

void Producto::setPosicion(int value)
{
    posicion = value;
}
