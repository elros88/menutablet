#ifndef PRODUCTOS_H
#define PRODUCTOS_H

#include <QObject>
#include <QAbstractListModel>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDebug>

#include "producto.h"

class Productos : public QAbstractListModel
{

    Q_OBJECT
    Q_PROPERTY(QJsonArray jsonProductos READ getJsonProductos WRITE setJsonProductos)
    Q_PROPERTY(QJsonArray jsonDetalles READ getJsonDetalles WRITE setJsonDetalles)
    Q_PROPERTY(QString pregunta1 READ getPregunta1 WRITE setPregunta1)
    Q_PROPERTY(QString pregunta2 READ getPregunta2 WRITE setPregunta2)
    Q_PROPERTY(QString pregunta3 READ getPregunta3 WRITE setPregunta3)
    Q_PROPERTY(QString pregunta4 READ getPregunta4 WRITE setPregunta4)
    Q_PROPERTY(int numeroPregunta READ getNumeroPregunta WRITE setNumeroPregunta)
    Q_PROPERTY(QString precioProducto READ getPrecio)

private:
    QList<Producto*> producto;
    QJsonArray jsonProductos;
    QJsonArray jsonDetalles;
    QVector <QString>preguntas;
    QString pregunta1;
    QString pregunta2;
    QString pregunta3;
    QString pregunta4;
    int numeroPregunta;
    QString precioProducto;

public:
    Productos(QObject *parent = 0);

    enum roles
    {
        codigo = Qt::UserRole + 1,
        nombre,
        precio,
        descripcion,
        posicion
    };

    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    QJsonArray getJsonProductos() const;
    void setJsonProductos(const QJsonArray &value);

    Q_INVOKABLE void iniciar ();
    Q_INVOKABLE void agregarProducto(QJsonObject jsonOrden, int posicion);
    Q_INVOKABLE void finalizar();

    Q_INVOKABLE QString getDescripcion(int indice);
    Q_INVOKABLE QString getCodigo(int indice);
    QJsonArray getJsonDetalles() const;
    void setJsonDetalles(const QJsonArray &value);

    Q_INVOKABLE void agregarPregunta(QJsonObject jsonDetalle);
    Q_INVOKABLE void agregarDetalles();

    QString getPregunta1() const;
    void setPregunta1(const QString &value);

    QString getPregunta2() const;
    void setPregunta2(const QString &value);

    QString getPregunta3() const;
    void setPregunta3(const QString &value);

    QString getPregunta4() const;
    void setPregunta4(const QString &value);

    int getNumeroPregunta() const;
    void setNumeroPregunta(int value);

    QVector<QString> getPreguntas() const;
    void setPreguntas(const QVector<QString> &value);

    QString getPrecio() const;

signals:

public slots:
};

#endif // PRODUCTOS_H
