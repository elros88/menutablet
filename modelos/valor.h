#ifndef VALOR_H
#define VALOR_H

#include <QObject>
#include <QDebug>


class Valor : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString codigo READ getCodigo WRITE setCodigo)
    Q_PROPERTY(QString nombre READ getNombre WRITE setNombre)
    Q_PROPERTY(bool elegido READ getElegido WRITE setElegido NOTIFY elegidoCambio)


    QString codigo;
    QString nombre;
    bool elegido;

public:

    explicit Valor(QObject *parent = 0);

    QString getCodigo() const;
    void setCodigo(const QString &value);
    QString getNombre() const;
    void setNombre(const QString &value);

    bool getElegido() const;
    void setElegido(const bool &value);

signals:
    void elegidoCambio();

public slots:
};

#endif // VALOR_H
