#include "pedidos.h"

bool Pedidos::getLlevar() const
{
    return llevar;
}

void Pedidos::setLlevar(bool value)
{
    llevar = value;
}

Pedidos::Pedidos(QObject *parent) : QAbstractListModel(parent)
{
    llevar = false;
}

QHash<int, QByteArray> Pedidos::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[numeroOrden] = "numeroOrden";
    roles[numeroProducto] = "numeroProducto";
    roles[nombreProducto] = "nombreProducto";
    roles[precioProducto] = "precioProducto";
    roles[numeroPregunta1] = "numeroPregunta1";
    roles[numeroPregunta2] = "numeroPregunta2";
    roles[numeroPregunta3] = "numeroPregunta3";
    roles[numeroPregunta4] = "numeroPregunta4";
    roles[numeroValor1] = "numeroValor1";
    roles[numeroValor2] = "numeroValor2";
    roles[numeroValor3] = "numeroValor3";
    roles[numeroValor4] = "numeroValor4";
    return roles;
}

QVariant Pedidos::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= pedido.count())
    {
        return QVariant();
    }

    switch (role)
    {
        case numeroOrden:
            return pedido[index.row()]->getNumeroOrden();
        case numeroProducto:
            return pedido[index.row()]->getNumeroProducto();
        case nombreProducto:
            return pedido[index.row()]->getNombreProducto();
        case precioProducto:
            return pedido[index.row()]->getPrecioProducto();
        case numeroPregunta1:
            return pedido[index.row()]->getNumeroPregunta1();
        case numeroPregunta2:
            return pedido[index.row()]->getNumeroPregunta2();
        case numeroPregunta3:
            return pedido[index.row()]->getNumeroPregunta3();
        case numeroPregunta4:
            return pedido[index.row()]->getNumeroPregunta4();
        case numeroValor1:
            return pedido[index.row()]->getNumeroValor1();
        case numeroValor2:
            return pedido[index.row()]->getNumeroValor2();
        case numeroValor3:
            return pedido[index.row()]->getNumeroValor3();
        case numeroValor4:
            return pedido[index.row()]->getNumeroValor4();

        default:
            break;
    }

    return QVariant();
}

int Pedidos::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return pedido.count();
}

void Pedidos::agregarPedido(QString numeroOrden,
                            QString numeroProducto,
                            QString nombreProducto,
                            QString precioProducto,
                            QString numeroPregunta1,
                            QString numeroPregunta2,
                            QString numeroPregunta3,
                            QString numeroPregunta4,
                            QString numeroValor1,
                            QString numeroValor2,
                            QString numeroValor3,
                            QString numeroValor4)
{
    beginInsertRows(QModelIndex(),pedido.length(), pedido.length());

    pedido.append(new Pedido);
    pedido[pedido.length()-1]->setNumeroOrden(numeroOrden);
    pedido[pedido.length()-1]->setNumeroProducto(numeroProducto);
    pedido[pedido.length()-1]->setNombreProducto(nombreProducto);
    pedido[pedido.length()-1]->setPrecioProducto(precioProducto);
    pedido[pedido.length()-1]->setNumeroPregunta1(numeroPregunta1);
    pedido[pedido.length()-1]->setNumeroPregunta2(numeroPregunta2);
    pedido[pedido.length()-1]->setNumeroPregunta3(numeroPregunta3);
    pedido[pedido.length()-1]->setNumeroPregunta4(numeroPregunta4);
    pedido[pedido.length()-1]->setNumeroValor1(numeroValor1);
    pedido[pedido.length()-1]->setNumeroValor2(numeroValor2);
    pedido[pedido.length()-1]->setNumeroValor3(numeroValor3);
    pedido[pedido.length()-1]->setNumeroValor4(numeroValor4);

    endInsertRows();
}

