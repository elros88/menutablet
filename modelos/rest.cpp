#include "rest.hpp"

QString Rest::getTipoRespuesta() const
{
    return tipoRespuesta;
}

void Rest::setTipoRespuesta(const QString &value)
{
    tipoRespuesta = value;
}

Rest::Rest(QObject *parent) : QObject(parent)
{
    estadoRespuesta = false;
}

Rest::~Rest()
{

}

//***** INITIALIZATE REQUEST PARAMETERS AND URL
void Rest::inicializar(QString url, QString token)
{
     qDebug()<<url;
     administradorConexion = new  QNetworkAccessManager(this);
     estadoRespuesta = false;
     peticion.setUrl(QUrl(url));
     peticion.setHeader(peticion.ContentTypeHeader,"application/json");

     if(!token.isEmpty())
     {
         QString auth = "Bearer " + token;
         peticion.setRawHeader(QByteArray("Authorization"), QByteArray(auth.toStdString().c_str()));
     }

}

//***** POST-REQUEST OPERATIONS *****
//       (in order of calling)
void Rest::respuestaTerminada()
{
    respuesta = qobject_cast<QNetworkReply*>(sender());
    setEstadoRespuesta(true);
}

bool Rest::validarRespuesta()
{
    if(respuesta->error() == QNetworkReply::NoError)
    {
        respuestaString = (QString)respuesta->readAll();
        respuestaJson = QJsonDocument::fromJson(respuestaString.toUtf8());
        qDebug()<<"Respuesta del Endpoint: "<<respuestaString.toStdString().c_str();
        //qDebug()<<"JSON: "<<respuestaJson.toJson();
        if(!respuestaJson.isArray())
        {
            objetoJson = respuestaJson.object();
            respuesta->deleteLater();
            qDebug()<<"objecto";
            return true;
        }
        if(respuestaJson.isArray())
        {
            arregloJson = respuestaJson.array();
            respuesta->deleteLater();
            //qDebug()<<"arreglo de "<<arregloJson.size();
            return true;
        }
    }
    else
    {
        respuestaString = respuesta->readAll();
        respuestaJson = QJsonDocument::fromJson(respuestaString.toUtf8());
        error = respuestaJson.object();
        QByteArray errorBody = QJsonDocument::fromJson(respuestaString.toUtf8()).toJson();
        qDebug()<<"Error en Respuesta: "<<respuestaString.toStdString().c_str();
        qDebug()<<"Error json: "<<respuestaJson.toJson();
        qDebug()<<errorBody.toStdString().c_str();

        respuesta->deleteLater();

    }
    return false;
}

void Rest::setEstadoRespuesta(bool status)
{
    if(estadoRespuesta != status)
    {
        estadoRespuesta = status;
        emit estadoRespuestaCambio();
        estadoRespuesta = !status;
    }

}

bool Rest::getEstadoRespuesta()
{
    return estadoRespuesta;
}

QString Rest::getError()
{
    return respuestaString;
}

QJsonArray Rest::getArregloJson() {

    return arregloJson;
}

QJsonObject Rest::getObjetoJson()
{
    return objetoJson;
}

QString Rest::getRespuestaString() const
{
    return respuestaString;
}

void Rest::setRespuestaString(const QString &value)
{
    respuestaString = value;
}

int Rest::getContador() const
{
    return contador;
}

void Rest::setContador(int value)
{
    contador = value;
    emit contadorCambio();
}

//***** END POST-REQUEST OPERATION *****

//***** REQUEST METHODS: POST, GET, PUT *****
void Rest::post(QByteArray data)
{
    respuesta = administradorConexion->post(peticion, data);
    connect(respuesta, SIGNAL(finished()), this, SLOT(respuestaTerminada()));
}

void Rest::get()
{
    respuesta = administradorConexion->get(peticion);
    connect(respuesta, SIGNAL(finished()), this, SLOT(respuestaTerminada()));
}

void Rest::put()
{
    QByteArray data;
    data.append("{}");
    respuesta = administradorConexion->put(peticion, data);
    connect(respuesta, SIGNAL(finished()), this, SLOT(respuestaTerminada()));
}

//***** END REQUEST METHODS *****
