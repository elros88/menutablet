#ifndef RESTCONNECTOR_HPP
#define RESTCONNECTOR_HPP

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QByteArray>
#include <QDebug>


class Rest : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool estadoRespuesta READ getEstadoRespuesta WRITE setEstadoRespuesta NOTIFY estadoRespuestaCambio)
    Q_PROPERTY(QString respuestaString READ getRespuestaString WRITE setRespuestaString NOTIFY respuestaStringCambio)
    Q_PROPERTY(int contador READ getContador WRITE setContador NOTIFY contadorCambio)
    Q_PROPERTY(QString tipoRespuesta READ getTipoRespuesta WRITE setTipoRespuesta NOTIFY tipoRespuestaCambio)

private:

    //**** ATTRIBUTES *******
    QEventLoop events;
    QNetworkAccessManager* administradorConexion;
    QNetworkRequest peticion;
    QNetworkReply* respuesta;
    QString respuestaString;
    QJsonObject error;
    QJsonDocument respuestaJson;
    QJsonObject objetoJson;
    QJsonArray arregloJson;
    QString tipoRespuesta;
    int contador;

    bool estadoRespuesta;
    //***** END ATTRIBUTES *****


public:

    //***** METHODS ********

    explicit Rest(QObject *parent = 0);

    Q_INVOKABLE void inicializar(QString url, QString token);
    Q_INVOKABLE void post (QByteArray data);
    Q_INVOKABLE void get ();
    Q_INVOKABLE void put ();
    Q_INVOKABLE bool validarRespuesta();

    //***** GETERS
    Q_INVOKABLE QString getError();
    Q_INVOKABLE QJsonObject getObjetoJson();
    Q_INVOKABLE QJsonArray getArregloJson();

    bool getEstadoRespuesta();

    //***** END GETTERS

    //***** SETERS
    Q_INVOKABLE void setEstadoRespuesta(bool status);
    //***** END SETTERS

    ~Rest();
    //***** END METHODS ***********


    QString getRespuestaString() const;
    void setRespuestaString(const QString &value);

    int getContador() const;
    void setContador(int value);


    QString getTipoRespuesta() const;
    void setTipoRespuesta(const QString &value);

signals:

    //void sslErrors(QNetworkReply*,QList<QSslError>);
    void estadoRespuestaCambio();
    void respuestaStringCambio();
    void contadorCambio();
    void tipoRespuestaCambio();

public slots:
   void respuestaTerminada();

};

#endif // RESTCONNECTOR_H
