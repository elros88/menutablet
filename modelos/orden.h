#ifndef ORDEN_H
#define ORDEN_H

#include <QObject>
#include "productos.h"

class Orden : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString codigo READ getCodigo WRITE setCodigo NOTIFY codigoChanged)
    Q_PROPERTY(int posicion READ getPosicion WRITE setPosicion NOTIFY posicionChanged)
    Q_PROPERTY(QString descripcion READ getDescripcion WRITE setDescripcion NOTIFY descripcionChanged)
private:
    QString codigo;
    int posicion;
    QString descripcion;
    Productos *productos;

public:
    explicit Orden(QObject *parent = 0);

    QString getCodigo() const;
    void setCodigo(const QString &value);

    int getPosicion() const;
    void setPosicion(int value);

    QString getDescripcion() const;
    void setDescripcion(const QString &value);

    Productos *getProductos() const;
    void setProductos(Productos *value);

signals:

    void codigoChanged();
    void posicionChanged();
    void descripcionChanged();

public slots:
};

#endif // ORDEN_H
