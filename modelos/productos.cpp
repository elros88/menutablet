#include "productos.h"

QJsonArray Productos::getJsonDetalles() const
{
    return jsonDetalles;
}

void Productos::setJsonDetalles(const QJsonArray &value)
{
    jsonDetalles = value;
}

QString Productos::getPregunta1() const
{
    return pregunta1;
}

void Productos::setPregunta1(const QString &value)
{
    pregunta1 = value;
}

QString Productos::getPregunta2() const
{
    return pregunta2;
}

void Productos::setPregunta2(const QString &value)
{
    pregunta2 = value;
}

QString Productos::getPregunta3() const
{
    return pregunta3;
}

void Productos::setPregunta3(const QString &value)
{
    pregunta3 = value;
}

QString Productos::getPregunta4() const
{
    return pregunta4;
}

void Productos::setPregunta4(const QString &value)
{
    pregunta4 = value;
}

int Productos::getNumeroPregunta() const
{
    return numeroPregunta;
}

void Productos::setNumeroPregunta(int value)
{
    numeroPregunta = value;
}

QVector<QString> Productos::getPreguntas() const
{
    return preguntas;
}

void Productos::setPreguntas(const QVector<QString> &value)
{
    preguntas = value;
}

QString Productos::getPrecio() const
{
    return precioProducto;
}

Productos::Productos(QObject *parent)
    : QAbstractListModel(parent)
{
    
}

QHash<int, QByteArray> Productos::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[codigo] = "codigo";
    roles[nombre] = "nombre";
    roles[precio] = "precio";
    roles[descripcion] = "descripcion";
    roles[posicion] = "posicion";
    return roles;
}

QVariant Productos::data(const QModelIndex & index, int role) const {

    if (index.row() < 0 || index.row() >= producto.count())
    {
        return QVariant();
    }

    switch (role)
    {
        case codigo:
            return producto[index.row()]->getCodigo();
        case nombre:
            return producto[index.row()]->getNombre();
        case precio:
            return producto[index.row()]->getPrecio();
        case descripcion:
            return producto[index.row()]->getDescripcion();
        case posicion:
            return producto[index.row()]->getPosicion();
        default:
            break;

    }

    return QVariant();
}

int Productos::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent);
    return producto.count();
}

QJsonArray Productos::getJsonProductos() const
{
    return jsonProductos;
}

void Productos::setJsonProductos(const QJsonArray &value)
{
    jsonProductos = value;
}

void Productos::iniciar()
{
    for(int i=0; i<jsonProductos.size() ; i++)
    {
        QJsonObject jsonProducto = jsonProductos[i].toObject();
        agregarProducto(jsonProducto, i);
    }

}

void Productos::agregarProducto(QJsonObject jsonProducto, int posicion)
{
    beginInsertRows(QModelIndex(), posicion, posicion);
    producto.append((new Producto));
    producto[posicion]->setDescripcion(jsonProducto.value("descript").toString());
    producto[posicion]->setCodigo(jsonProducto.value("prodnum").toString());
    endInsertRows();
}

void Productos::finalizar()
{
    while(producto.count()>0)
    {
        beginRemoveRows(QModelIndex(),0,0);
        producto.pop_front();
        endRemoveRows();
    }
}

QString Productos::getDescripcion(int indice)
{
    return producto[indice]->getDescripcion();
}

QString Productos::getCodigo(int indice)
{
    return producto[indice]->getCodigo();
}

void Productos::agregarDetalles()
{
    for(int i=0; i<jsonDetalles.size() ; i++)
    {
        QJsonObject jsonDetalle = jsonDetalles[i].toObject();
        agregarPregunta(jsonDetalle);
    }
}

void Productos::agregarPregunta(QJsonObject jsonDetalle)
{

    if(!jsonDetalle.value("pricea").isNull())
    {
        precioProducto = jsonDetalle.value("pricea").toString();
    }
    else
    {
        precioProducto = " ";
    }
    if(jsonDetalle.value("question1").toString() != "0" && !jsonDetalle.value("question1").isNull())
    {
        pregunta1 = jsonDetalle.value("question1").toString();
    }
    else
    {
        pregunta1 = "0";
    }

    if(jsonDetalle.value("question2").toString() != "0" && !jsonDetalle.value("question2").isNull())
    {
        pregunta2 = jsonDetalle.value("question2").toString();
    }
    else
    {
        pregunta2 = "0";
    }

    if(jsonDetalle.value("question3").toString() != "0" && !jsonDetalle.value("question3").isNull())
    {
        pregunta3 = jsonDetalle.value("question3").toString();
    }
    else
    {
        pregunta3 = "0";
    }

    if(jsonDetalle.value("question4").toString() != "0" && !jsonDetalle.value("question4").isNull())
    {
        pregunta4 = jsonDetalle.value("question4").toString();
    }
    else
    {
        pregunta4 = "0";
    }
}
