#ifndef VALORES_H
#define VALORES_H

#include <QObject>
#include <QAbstractListModel>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDebug>
#include "modelos/valor.h"

class Valores : public QAbstractListModel
{
    Q_OBJECT

    QList<Valor*> valor;
    QJsonArray json;

public:
    explicit Valores(QObject *parent = 0);

    enum roles{
        codigo = Qt::UserRole + 1,
        nombre,
        elegido
    };

    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    void iniciar(QJsonArray json);
    void agregarValor(int posicion, QJsonObject jsonValor);
    Q_INVOKABLE void elegir(int posicion);
    QString leerCodigo(int posicion);
signals:

public slots:
};

#endif // VALORES_H
