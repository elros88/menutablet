#include "orden.h"

Productos *Orden::getProductos() const
{
    return productos;
}

void Orden::setProductos(Productos *value)
{
    productos = value;
}

Orden::Orden(QObject *parent) : QObject(parent)
{
    
}

QString Orden::getDescripcion() const
{
    return descripcion;
}

void Orden::setDescripcion(const QString &value)
{
    descripcion = value;
    emit descripcionChanged();
}

int Orden::getPosicion() const
{
    return posicion;
}

void Orden::setPosicion(int value)
{
    posicion = value;
    emit posicionChanged();
}

QString Orden::getCodigo() const
{
    return codigo;
}

void Orden::setCodigo(const QString &value)
{
    codigo = value;
    emit codigoChanged();
}
