#include "valor.h"

bool Valor::getElegido() const
{
    return elegido;
}

void Valor::setElegido(const bool &value)
{
    qDebug()<<value;
    elegido = value;
    emit elegidoCambio();
}

Valor::Valor(QObject *parent) : QObject(parent)
{

}

QString Valor::getNombre() const
{
    return nombre;
}

void Valor::setNombre(const QString &value)
{
    nombre = value;
}

QString Valor::getCodigo() const
{
    return codigo;
}

void Valor::setCodigo(const QString &value)
{
    codigo = value;
}
