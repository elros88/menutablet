#ifndef PRODUCTO_H
#define PRODUCTO_H

#include <QObject>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

class Producto : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString codigo READ getCodigo WRITE setCodigo NOTIFY codigoChanged)
    Q_PROPERTY(QString nombre READ getNombre WRITE setNombre NOTIFY nombreChanged)
    Q_PROPERTY(float precio READ getPrecio WRITE setPrecio NOTIFY precioChanged)
    Q_PROPERTY(int posicion READ getPosicion WRITE setPosicion NOTIFY posicionChanged)
    Q_PROPERTY(QString descripcion READ getDescripcion WRITE setDescripcion NOTIFY descripcionChanged)

private:

    QString codigo;
    QString nombre;
    QString descripcion;
    float precio;
    int posicion;

public:
    explicit Producto(QObject *parent = 0);


    QString getDescripcion() const;
    void setDescripcion(const QString &value);

    float getPrecio() const;
    void setPrecio(float value);

    QString getCodigo() const;
    void setCodigo(const QString &value);

    QString getNombre() const;
    void setNombre(const QString &value);

    int getPosicion() const;
    void setPosicion(int value);

signals:

    void codigoChanged();
    void descripcionChanged();
    void precioChanged();
    void nombreChanged();
    void posicionChanged();
    void codigoPreguntaChanged();

public slots:
};

#endif // PRODUCTO_H
