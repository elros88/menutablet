#ifndef PEDIDO_H
#define PEDIDO_H

#include <QObject>

class Pedido : public QObject
{
    Q_OBJECT

    QString numeroOrden;
    QString numeroProducto;
    QString nombreProducto;
    QString precioProducto;
    QString numeroPregunta1;
    QString numeroPregunta2;
    QString numeroPregunta3;
    QString numeroPregunta4;
    QString numeroValor1;
    QString numeroValor2;
    QString numeroValor3;
    QString numeroValor4;

public:
    explicit Pedido(QObject *parent = 0);

    QString getNumeroOrden() const;
    void setNumeroOrden(const QString &value);

    QString getNumeroProducto() const;
    void setNumeroProducto(const QString &value);

    QString getNombreProducto() const;
    void setNombreProducto(const QString &value);

    QString getPrecioProducto() const;
    void setPrecioProducto(const QString &value);

    QString getNumeroPregunta1() const;
    void setNumeroPregunta1(const QString &value);

    QString getNumeroPregunta2() const;
    void setNumeroPregunta2(const QString &value);

    QString getNumeroPregunta3() const;
    void setNumeroPregunta3(const QString &value);

    QString getNumeroPregunta4() const;
    void setNumeroPregunta4(const QString &value);

    QString getNumeroValor1() const;
    void setNumeroValor1(const QString &value);

    QString getNumeroValor2() const;
    void setNumeroValor2(const QString &value);

    QString getNumeroValor3() const;
    void setNumeroValor3(const QString &value);

    QString getNumeroValor4() const;
    void setNumeroValor4(const QString &value);

signals:

public slots:
};

#endif // PEDIDO_H
