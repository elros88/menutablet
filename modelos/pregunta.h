#ifndef PREGUNTA_H
#define PREGUNTA_H

#include <QObject>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDebug>


#include "valores.h"

class Pregunta : public QObject
{
    Q_OBJECT

public:
    explicit Pregunta(int numero, QObject *parent = 0);

    Valores *valores;
    int numero;
    bool visible;

    Valores *getValores() const;
    void setValores(Valores *value);
    void guardarValores(QJsonArray json);
    void elegirValor(int posicion);
    QString leerCodigo(int posicion);

    int getNumero() const;
    void setNumero(int value);

    bool getVisible() const;
    void setVisible(bool value);

signals:

public slots:
};

#endif // PREGUNTA_H
