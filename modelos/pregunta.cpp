#include "pregunta.h"

Pregunta::Pregunta(int numero, QObject *parent) : QObject(parent)
{
    valores = new Valores();
    setNumero(numero);
    setVisible(false);
}

bool Pregunta::getVisible() const
{
    return visible;
}

void Pregunta::setVisible(bool value)
{
    visible = value;
}

int Pregunta::getNumero() const
{
    return numero;
}

void Pregunta::setNumero(int value)
{
    numero = value;
}

Valores *Pregunta::getValores() const
{
    return valores;
}

void Pregunta::setValores(Valores *value)
{
    valores = value;
}

void Pregunta::guardarValores(QJsonArray json)
{
    qDebug()<<"Guardando Valores de la pregunta";
    setVisible(true);
    valores->iniciar(json);
}

void Pregunta::elegirValor(int posicion)
{
    valores->elegir(posicion);
}

QString Pregunta::leerCodigo(int posicion)
{
   return valores->leerCodigo(posicion);
}
