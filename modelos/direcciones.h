#ifndef DIRECCIONES_H
#define DIRECCIONES_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QDebug>

class Direcciones : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString base READ getBase WRITE setBase)
    Q_PROPERTY(QString ordenes READ getOrdenes)
    Q_PROPERTY(QString productos READ getProductos)
    Q_PROPERTY(QString detalles READ getDetalles)
    Q_PROPERTY(QString preguntas READ getPreguntas)
    Q_PROPERTY(QString login READ getLogin)
    Q_PROPERTY(bool errorLectura READ getErrorLectura WRITE setErrorLectura NOTIFY errorLecturaCambio)

    QString base;
    QString ordenes;
    QString productos;
    QString detalles;
    QString preguntas;
    QString login;
    QFile ip;
    bool errorLectura;

public:
    explicit Direcciones(QObject *parent = 0);
    QString getOrdenes() const;
    QString getProductos() const;
    QString getDetalles() const;
    QString getPreguntas() const;
    QString getLogin() const;
    QString getBase() const;
    void setBase(const QString &value);
    bool getErrorLectura() const;
    void setErrorLectura(bool value);

signals:
    void errorLecturaCambio();
};

#endif // DIRECCIONES_H
