import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

import "qrc:/controladores/controladorMenu.js" as Controlador

Item
{
    //property real indexValor: -1
    width: parent.width
    height: parent.height

    property string numOrden : " "
    property string numProducto : " "
    property string nombreProducto : " "
    property string precioProducto : " "
    property string pregunta1 : " "
    property string pregunta2 : " "
    property string pregunta3 : " "
    property string pregunta4 : " "
    property string valor1 : " "
    property string valor2 : " "
    property string valor3 : " "
    property string valor4 : " "


    Connections
    {
         target: zonaCarga
         onVistaChanged: Controlador.pedirOrdenes()
    }

    Connections
    {
        target: rest
        onEstadoRespuestaCambio: Controlador.recibirOrdenes()
    }

    Image
    {
        id: logo
        source: "qrc:/recursos/logo"
        height: parent.height*0.2
        fillMode: Image.PreserveAspectFit
        anchors.top: parent.top
        anchors.topMargin: parent.height*0.025
        anchors.left: parent.left
        anchors.leftMargin: parent.height*0.025
    }


    RowLayout
    {
        width: parent.width
        height: parent.height*0.7
        spacing: 8

        anchors.top: logo.bottom
        anchors.topMargin:  parent.height*0.025

        Item
        {
            id: areaPedido
            z: 2
            Layout.fillHeight: true
            Layout.maximumWidth: parent.width*0.4
            Layout.minimumWidth: parent.width*0.4
            Layout.preferredWidth: parent.width*0.4
            Loader
            {
                id: cargadorAreaPedido
                anchors.fill: parent
                source: "qrc:/areaPedido"
            }


        }

        Item
        {
            id: areaMenu
            Layout.fillHeight: true
            Layout.maximumWidth: parent.width*0.55
            Layout.minimumWidth: parent.width*0.55
            Layout.preferredWidth: parent.width*0.55

            Loader
            {
                id: cargadorAreaMenu
                anchors.fill: parent
                source:"qrc:/areaMenu"
            }
        }
    }


    Rectangle
    {
        id: detalleProducto
        width: parent.width*0.75
        height: parent.height*0.75
        color:"black"
        border.width: 2
        border.color: "white"
        anchors.centerIn: parent
        visible: false

        Connections
        {
            target: restDetalles
            onEstadoRespuestaCambio: Controlador.recibirDetalles()
        }

        Connections
        {
            target: restPreguntas
            onEstadoRespuestaCambio: Controlador.recibirPregunta()
        }

        Text
        {
            id: nombreProducto
            width: parent.width*0.3
            anchors.right: parent.right
            anchors.rightMargin: parent.width*0.1
            anchors.top: parent.top
            anchors.topMargin: parent.height*0.06
            color: "white"
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignRight
        }

        Item
        {
            id: areaPreguntas
            width: parent.width*0.9
            height: parent.height * 0.7

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: nombreProducto.bottom
            anchors.topMargin: parent.height*0.06

            ColumnLayout
            {
                anchors.fill: parent
                spacing: 1
                Image
                {
                    id: fila0
                    visible: false
                    Layout.fillWidth: true
                    Layout.maximumHeight: parent.height*0.2
                    width: parent.width
                    height: parent.height
                    source: "qrc:/recursos/textura"

                    ExclusiveGroup
                    {
                        id: grupoValor0
                    }
                    ListView
                    {
                        id: listaValores0
                        anchors.fill: parent
                        delegate: valor0
                        clip: true
                        orientation: ListView.Horizontal

                    }
                    Component
                    {
                        id: valor0

                        Item
                        {
                            width: listaValores0.width*0.2
                            height: listaValores0.height*0.4

                            RadioButton
                            {
                                id: check
                                anchors.left: parent.left
                                anchors.leftMargin: 3
                                checked: elegido
                                onClicked: Controlador.guardarValor(index, 0)
                                exclusiveGroup: grupoValor0
                            }

                            Text
                            {
                               id: opcionValor
                               text: nombre
                               color: "white"
                               width: parent.width*0.9
                               wrapMode: Text.WordWrap
                               anchors.left: check.right
                               anchors.leftMargin: 3
                             }
                        }

                    }

                }

                Image
                {
                    id: fila1
                    visible: false
                    Layout.fillWidth: true
                    Layout.maximumHeight: areaPreguntas.height*0.2
                    width: parent.width
                    height: parent.height
                    source: "qrc:/recursos/textura"

                    ExclusiveGroup
                    {
                        id: grupoValor1
                    }
                    ListView
                    {
                        id: listaValores1
                        anchors.fill: parent
                        delegate: valor1
                        clip: true
                        orientation: ListView.Horizontal

                    }
                    Component
                    {
                        id: valor1

                        Item
                        {
                            width: listaValores1.width*0.2
                            height: listaValores1.height*0.4

                            RadioButton
                            {
                                id: check
                                anchors.left: parent.left
                                anchors.leftMargin: 3
                                checked: elegido
                                onClicked: Controlador.guardarValor(index, 1)
                                exclusiveGroup: grupoValor1
                            }

                            Text
                            {
                               id: opcionValor
                               text: nombre
                               color: "white"
                               width: parent.width*0.9
                               wrapMode: Text.WordWrap
                               anchors.left: check.right
                               anchors.leftMargin: 3
                             }
                        }

                    }

                }

                Image
                {
                    id: fila2
                    visible: false
                    Layout.fillWidth: true
                    Layout.maximumHeight: parent.height*0.2
                    width: parent.width
                    height: parent.height
                    source: "qrc:/recursos/textura"


                    ExclusiveGroup
                    {
                        id: grupoValor2
                    }

                    ListView
                    {
                        id: listaValores2
                        anchors.fill: parent
                        delegate: valor2
                        clip: true
                        orientation: ListView.Horizontal

                    }
                    Component
                    {
                        id: valor2

                        Item
                        {
                            width: listaValores2.width*0.2
                            height: listaValores2.height*0.4

                            RadioButton
                            {
                                id: check
                                anchors.left: parent.left
                                anchors.leftMargin: 3
                                checked: elegido
                                onClicked: Controlador.guardarValor(index, 2)
                                exclusiveGroup: grupoValor2
                            }

                            Text
                            {
                               id: opcionValor
                               text: nombre
                               color: "white"
                               width: parent.width*0.9
                               wrapMode: Text.WordWrap
                               anchors.left: check.right
                               anchors.leftMargin: 3
                             }
                        }

                    }


                }

                Image
                {
                    id: fila3
                    visible: false
                    Layout.fillWidth: true
                    Layout.maximumHeight: parent.height*0.2
                    width: parent.width
                    height: parent.height
                    source: "qrc:/recursos/textura"


                    ExclusiveGroup
                    {
                        id: grupoValor3
                    }
                    ListView
                    {
                        id: listaValores3
                        anchors.fill: parent
                        delegate: valor3
                        clip: true
                        orientation: ListView.Horizontal

                    }
                    Component
                    {
                        id: valor3

                        Item
                        {
                            width: listaValores1.width*0.2
                            height: listaValores1.height*0.4

                            RadioButton
                            {
                                id: check
                                anchors.left: parent.left
                                anchors.leftMargin: 3
                                checked: elegido
                                onClicked: Controlador.guardarValor(index, 3)
                                exclusiveGroup: grupoValor3
                            }

                            Text
                            {
                               id: opcionValor
                               text: nombre
                               color: "white"
                               width: parent.width*0.9
                               wrapMode: Text.WordWrap
                               anchors.left: check.right
                               anchors.leftMargin: 3
                             }
                        }

                    }

                }
            }

            MessageDialog
            {
                id: error
                visible: false
                title: qsTr("Error")
                onAccepted:
                {
                    error.visible = false
                }
            }
        }

        Image {
            id: guardar
            source: "qrc:/recursos/aceptar"
            width: parent.width*0.1
            height: width
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.rightMargin: parent.width*0.05
            anchors.bottomMargin: parent.height*0.05
            fillMode: Image.PreserveAspectFit

            MouseArea
            {
                anchors.fill: parent
                onClicked: Controlador.guardarPedido()
            }
        }

    }


}

