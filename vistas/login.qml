import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtMultimedia 5.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

import "qrc:/controladores/controladorLogin.js" as Controlador

Item {

    id: vistaLogin
    anchors.fill: parent

    Connections
    {
        target: botonAceptar
        onClicked: Controlador.iniciarSesion()
    }

    Connections
    {
        target: rest
        onEstadoRespuestaCambio: Controlador.respuesta()
    }

    RowLayout
    {
        anchors.fill: parent

        Item
        {

            id: areaLogo
            Layout.preferredWidth: parent.width*0.5
            Layout.preferredHeight: parent.height

            Image
            {
                id: logo
                width: parent.width*0.7
                fillMode: Image.PreserveAspectFit
                source: "recursos/logo"
                anchors.centerIn: parent
            }
        }


        Item
        {
            id: areaLogin
            Layout.preferredWidth: parent.width*0.5
            Layout.preferredHeight: parent.height

            Image
            {
                id: fondoArea
                width: vistaLogin.width*0.25
                height: menuKFC.height
                source: "qrc:/recursos/textura"
                //fillMode: Image.PreserveAspectFit
                anchors.centerIn: parent

                Text
                {
                    id: mensaje
                    text: qsTr("Ingresa aquí tu \n tu código de cajero")
                    font.pointSize: 12
                    color: "white"
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: contenedorCodigo.top
                    anchors.bottomMargin: parent.width*0.03
                    font.family: "ubuntu"
                    horizontalAlignment: Text.AlignHCenter
                }
                Rectangle
                {
                    id: contenedorCodigo
                    height: fondoArea.height*0.1
                    width: parent.width*0.9
                    color: "white"
                    anchors.centerIn: parent
                    TextField
                    {
                        id: codigo
                        width: parent.width*0.7
                        height: parent.height*0.8
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        inputMethodHints: Qt.ImhDigitsOnly
                        textColor: "black"
                        font.pointSize: 12

                    }
                }



                Image
                {
                    id: aceptar
                    source: "qrc:/recursos/aceptar"
                    width: areaLogin.width*0.1
                    fillMode: Image.PreserveAspectFit
                    anchors.top: contenedorCodigo.bottom
                    anchors.topMargin: parent.width*0.1
                    anchors.right : parent.right
                    anchors.rightMargin: parent.width*0.1



                    MouseArea
                    {
                        id: botonAceptar
                        anchors.fill: parent
                        onClicked: Controlador.iniciarSesion()
                    }
                }

            }
        }

    }


    Image
    {
        source: "qrc:/recursos/configuracion"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height*0.05
        anchors.left: parent.left
        anchors.leftMargin: parent.width*0.05
        width: parent.width*0.05
        height: width
        fillMode: Image.PreserveAspectFit
        MouseArea
        {
            anchors.fill: parent
            onClicked: Controlador.mostrarConfiguracion()
        }
    }

    Rectangle
    {
        id: mascara
        z:2
        width: parent.width
        height: parent.height
        visible: false
        color: "#80000000"
        BusyIndicator
        {
            id: cargando
            running: false
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
        }
    }

    MessageDialog
    {
        id: dialogo
        visible: false
        title: qsTr("Error")


//        Text {
//            id: mensajeError

//        }
        onAccepted:
        {
            dialogo.visible = false
        }
    }

    Dialog
    {

        id: configuracion
        visible: false
        title: qsTr("Configuración")
        Rectangle
        {
            implicitWidth: vistaLogin.width*0.65
            implicitHeight: vistaLogin.height*0.3

            RowLayout
            {
                width: parent.width*0.9
                anchors.left: parent.left
                anchors.leftMargin: parent.width*0.05
                height: parent.height
                anchors.centerIn: parent
                spacing: 3
                Text
                {
                    id: tituloIP
                    text: qsTr("IP servidor:")
                    anchors.verticalCenter: parent.verticalCenter
                }
                TextField
                {
                    id: ip
                    width: 200
                    anchors.verticalCenter: parent.verticalCenter
                    font.pointSize: 12
                    placeholderText: "255.255.255.255"
                    inputMethodHints: Qt.ImhDigitsOnly
                }
            }

        }


        onAccepted:Controlador.guardarConfiguracion()

        standardButtons: StandardButton.Ok | StandardButton.Cancel
    }


}

