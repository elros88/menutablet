import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1

import "qrc:/controladores/controladorMenu.js" as Controlador


Item
{
    anchors.fill: parent
    property bool salon: true
    property bool llevar: false

    Image
    {
        source: "qrc:/recursos/textura"
        width: parent.width*0.9
        height: parent.height
        anchors.horizontalCenter: parent.horizontalCenter
    }

    ColumnLayout
    {
        anchors.fill: parent
        spacing: 0

        Item
        {
            id: areaBotones
            Layout.fillWidth: true
            Layout.preferredHeight: parent.height*0.2
            z:2

           RowLayout
            {
                width: parent.width*0.6
                height: parent.height
                anchors.right: parent.right
                anchors.rightMargin: parent.width*0.05

                Item
                {
                    id: botonSalon

                    Layout.preferredWidth: parent.width*0.4
                    Layout.preferredHeight: parent.height*0.9

                    Text
                    {
                        id: textoSalon
                        text: qsTr("Salón")
                        color: "white"
                        font.family: "ubuntu"
                        anchors.centerIn: parent
                        font.pixelSize: parent.height*0.25
                    }

                    Rectangle
                    {
                        color: "white"
                        height: 3
                        width: textoSalon.width*1.2
                        anchors.top: textoSalon.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        visible: salon
                    }
                    MouseArea
                    {
                        anchors.fill: parent
                        onClicked: Controlador.elegirSalon()
                    }

                }

                Item
                {
                    id: botonLlevar

                    Layout.preferredWidth: parent.width*0.4
                    Layout.preferredHeight: parent.height*0.9

                    Text
                    {
                        id: textoLlevar
                        text: qsTr("Llevar")
                        color: "white"
                        font.family: "ubuntu"
                        anchors.centerIn: parent
                        font.pixelSize: parent.height*0.25
                    }

                    Rectangle
                    {
                        color: "white"
                        height: 3
                        width: textoLlevar.width*1.2
                        anchors.top: textoLlevar.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        visible: llevar
                    }

                    MouseArea
                    {
                        anchors.fill: parent
                        onClicked: Controlador.elegirLlevar()
                    }
                }
            }
        }

        Item
        {
            id: areaListaProductos
            Layout.fillWidth: true
            Layout.preferredHeight: parent.height*0.8

            ListView
            {
                width: parent.width*0.8
                height: parent.height*0.9
                anchors.centerIn: parent
                model: modeloPedidos
                delegate: listaPedido
                clip: true
            }

            Component
            {
                id: listaPedido

                Text
                {
                    text: nombreProducto
                    font.family: "ubuntu"
                    color: "white"
                    anchors.left: parent.left
                    anchors.leftMargin: parent.width*0.07
                }

             }


        }
    }

}

