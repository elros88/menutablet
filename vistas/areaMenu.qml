import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1
import QtGraphicalEffects 1.0

import "qrc:/controladores/controladorMenu.js" as Controlador
Item
{
    anchors.fill: parent

    Connections
    {
        target: restProductos
        onEstadoRespuestaCambio: Controlador.recibirProductos()
    }

    ColumnLayout
    {
        anchors.fill: parent

        Item
        {
            id: areaProductos
            Layout.fillWidth: true
            Layout.maximumHeight: parent.height*0.7
            Layout.minimumHeight: parent.height*0.7
            Layout.preferredHeight: parent.height*0.7

            GridView
            {
                id: listaProductos
                anchors.fill: parent
                delegate: producto
                model: modeloProductos
                cellWidth: width*0.20
                cellHeight: cellWidth*0.5

            }
            Component
            {
                id: producto

                Rectangle
                {
                    width: listaProductos.width*0.18
                    height: width*0.45
                    radius: 6
                    color: "#ff191919"
                    Text {
                        id: descripcionProducto
                        text: descripcion
                        color: "white"
                        anchors.centerIn: parent
                        width: parent.width*0.9
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: parent.height*0.20
                    }

                    MouseArea
                    {
                        anchors.fill: parent
                        onClicked: Controlador.mostrarProducto(index)
                    }
                }
            }

        }

        Item
        {
            id: areaOrdenes

            Layout.fillWidth: true
            Layout.maximumHeight: parent.height*0.3
            Layout.minimumHeight: parent.height*0.3
            Layout.preferredHeight: parent.height*0.3

            Image
            {
                source: "qrc:/recursos/textura"
                width: parent.width
                height: parent.height
            }

            ListView
            {
                id: listaOrdenes
                width: parent.width*0.95
                height: parent.height*0.95
                anchors.centerIn: parent
                model: modeloOrdenes
                clip: true
                delegate: orden
                orientation: ListView.Horizontal
            }

            Component
            {
                id: orden
                Item
                {
                    width: areaOrdenes.width*0.25
                    height: areaOrdenes.height

                    Text {
                        id: descripcionOrden
                        text: descripcion
                        color: "white"
                        anchors.centerIn: parent
                        width: parent.width*0.8
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: parent.height*0.11
                    }
                    MouseArea
                    {
                        anchors.fill: parent
                        onClicked: Controlador.pedirProductos(codigo)
                    }

                    Rectangle
                    {
                        id: indicador
                        width: parent.width*0.8
                        height: 3
                        color: "white"
                        anchors.top: descripcionOrden.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        visible: false

                    }
                }
            }
        }
    }
}

