#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlComponent>
#include <QDebug>

#include "modelos/orden.h"
#include "modelos/ordenes.h"
#include "modelos/productos.h"
#include "modelos/rest.hpp"
#include "modelos/direcciones.h"
#include "modelos/preguntas.h"
#include "modelos/pedidos.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    Ordenes modeloOrdenes;
    Productos modeloProductos;
    Preguntas modeloPreguntas;
    Pedidos modeloPedidos;
    Rest rest;
    Rest restProductos;
    Rest restDetalles;
    Rest restPreguntas;
    Direcciones direcciones;

    qDebug()<<"La direccion del server es: "<<direcciones.getBase();
    qDebug()<<"Hay error de lectura: "<<direcciones.getErrorLectura();

    qmlRegisterType<Productos>("com.menu.lista", 1, 0, "Productos");

    engine.rootContext()->setContextProperty("modeloOrdenes", &modeloOrdenes);
    engine.rootContext()->setContextProperty("modeloProductos", &modeloProductos);
    engine.rootContext()->setContextProperty("modeloPreguntas", &modeloPreguntas);
    engine.rootContext()->setContextProperty("modeloPedidos", &modeloPedidos);
    engine.rootContext()->setContextProperty("rest", &rest);
    engine.rootContext()->setContextProperty("restProductos", &restProductos);
    engine.rootContext()->setContextProperty("restDetalles", &restDetalles);
    engine.rootContext()->setContextProperty("restPreguntas", &restPreguntas);
    engine.rootContext()->setContextProperty("direcciones", &direcciones);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

