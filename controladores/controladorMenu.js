function pedirOrdenes()
{
    if(zonaCarga.vista === "MENU")
    {
        rest.inicializar(direcciones.ordenes, "");
        rest.get();
    }

}

function recibirOrdenes()
{
    if(rest.validarRespuesta())
    {
        modeloOrdenes.jsonOrdenes = rest.getArregloJson();
        modeloOrdenes.iniciar();
        pedirProductos(modeloOrdenes.getCodigo(0));
    }
    else
    {
        error.visible =  true;
        error.text = qsTr("Error recibiendo ordenes");
    }


}

function pedirProductos(codigoOrden)
{
    restProductos.inicializar(direcciones.productos+codigoOrden, "");
    restProductos.get();
    numOrden = codigoOrden;
    mostrarIndicadorOrden(1);
}

function recibirProductos()
{
    if(restProductos.validarRespuesta())
    {
        modeloProductos.finalizar();
        modeloProductos.jsonProductos = restProductos.getArregloJson();
        modeloProductos.iniciar();
    }

}

function pedirDetalles(indice)
{
    numProducto = modeloProductos.getCodigo(indice).toString();
    modeloProductos.numeroPregunta = 0;
    restDetalles.inicializar(direcciones.detalles+"/"+modeloProductos.getCodigo(indice).toString(), "");
    restDetalles.get();
}

function recibirDetalles()
{
    if(restDetalles.validarRespuesta())
    {
        modeloProductos.jsonDetalles = restDetalles.getArregloJson();
        console.log("Agregando detalle")
        modeloProductos.agregarDetalles();
        precioProducto = modeloProductos.precio;
        pregunta1 = modeloProductos.pregunta1;
        pregunta2 = modeloProductos.pregunta2;
        pregunta3 = modeloProductos.pregunta3;
        pregunta4 = modeloProductos.pregunta4;
        pedirPreguntas();
    }

}

function pedirPreguntas()
{
    var pregunta;
    var peticion = false;

    while(modeloProductos.numeroPregunta < 4 && !peticion)
    {
        switch(modeloProductos.numeroPregunta)
        {
        case 0:
            pregunta = modeloProductos.pregunta1;
            break;
        case 1:
            pregunta = modeloProductos.pregunta2;
            break;
        case 2:
            pregunta = modeloProductos.pregunta3
            break;
        case 3:
            pregunta = modeloProductos.pregunta4;
            break;
        }

        if(pregunta !== "0")
        {
            peticion = true;
            console.log("Pidiendo pregunta. "+ pregunta)
            restPreguntas.inicializar(direcciones.preguntas+"/"+pregunta, "");
            restPreguntas.get();
            break;
        }
        else
        {
            modeloProductos.numeroPregunta++;
        }
    }
}

function recibirPregunta()
{
    console.log("Recibiendo Preguntas")
    if(restPreguntas.validarRespuesta())
    {
        modeloPreguntas.json = restPreguntas.getArregloJson();
        modeloPreguntas.iniciar(modeloProductos.numeroPregunta);

    }
    else
    {
        console.log(restPreguntas.getError())
    }



    modeloProductos.numeroPregunta++;
    console.log(modeloProductos.numeroPregunta)

    if(modeloProductos.numeroPregunta < 4)
    {
        pedirPreguntas();
    }
    else
    {
        if(modeloPreguntas.verificarVisible(0))
        {
            fila0.visible = true;
            listaValores0.model = modeloPreguntas.getModelo(0);
        }
        if(modeloPreguntas.verificarVisible(1))
        {
            fila1.visible = true;
            listaValores1.model = modeloPreguntas.getModelo(1);
        }
        if(modeloPreguntas.verificarVisible(2))
        {
            fila2.visible = true;
            listaValores2.model = modeloPreguntas.getModelo(2);
        }
        if(modeloPreguntas.verificarVisible(3))
        {
            fila3.visible = true;
            listaValores3.model = modeloPreguntas.getModelo(3);
        }
    }
}

function mostrarProducto(indice)
{

    nombreProducto.text = modeloProductos.getDescripcion(indice);

    pedirDetalles(indice);

    detalleProducto.visible = true;
}

function elegirSalon()
{
    modeloPedidos.llevar = false;
    salon = true;
    llevar = false;
}

function elegirLlevar()
{
    modeloPedidos.llevar = true;
    salon = false;
    llevar = true;
}

function mostrarIndicadorOrden(indiceAnterior)
{
    listaOrdenes.currentItem.indicador.visible = true;

}

function guardarValor(indexValor, indexPregunta)
{
    switch(indexPregunta)
    {
    case 0:
        valor1 = modeloPreguntas.leerCodigoValor(indexValor, indexPregunta);
        break;
    case 1:
        valor2 = modeloPreguntas.leerCodigoValor(indexValor, indexPregunta);
        break;
    case 2:
        valor3 = modeloPreguntas.leerCodigoValor(indexValor, indexPregunta);
        break;
    case 3:
        valor4 = modeloPreguntas.leerCodigoValor(indexValor, indexPregunta);
        break;
    }

    modeloPreguntas.elegirValor(indexValor, indexPregunta)
}

function guardarPedido()
{
    modeloPedidos.agregarPedido(numOrden, numProducto, nombreProducto, precioProducto,
                                pregunta1, pregunta2, pregunta3, pregunta4,
                                valor1, valor2, valor3, valor4);
    numOrden = " ";
    numProducto = " ";
    nombreProducto = " ";
    precioProducto = " ";
    pregunta1 = " ";
    pregunta2 = " ";
    pregunta3 = " ";
    pregunta4 = " ";
    valor1 = " ";
    valor2 = " ";
    valor3 = " ";
    valor4 = " ";
}
