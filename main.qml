import QtQuick 2.2
import QtQuick.Window 2.0
import QtMultimedia 5.5
import QtGraphicalEffects 1.0

Window {

    id: menuKFC

    visible: true
    minimumHeight: 480
    minimumWidth: 800
    width: 800
    height: 480

    Rectangle {
        id: zonaCarga
        width: parent.width
        height: parent.height
        color: "black"


        property string vista


        states: [
            State {
                name: "LOGIN"
                PropertyChanges {
                    target: cargador
                    source: "qrc:/login"

                }
                PropertyChanges {
                    target: zonaCarga
                    vista: "LOGIN"

                }
            },
            State {
                name: "MENU"
                PropertyChanges {
                    target: cargador
                    source: "qrc:/menu"

                }
                PropertyChanges {
                    target: zonaCarga
                    vista: "MENU"

                }
            }
        ]

        Loader
        {
            id: cargador
            anchors.fill: parent
            source: "qrc:/login"

        }
    }
}

