TEMPLATE = app

QT += qml quick sql svg
CONFIG += c++11

SOURCES += main.cpp \
    modelos/orden.cpp \
    modelos/ordenes.cpp \
    modelos/producto.cpp \
    modelos/productos.cpp \
    modelos/rest.cpp \
    modelos/direcciones.cpp \
    modelos/pregunta.cpp \
    modelos/preguntas.cpp \
    modelos/valor.cpp \
    modelos/valores.cpp \
    modelos/pedido.cpp \
    modelos/pedidos.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

HEADERS += \
    modelos/orden.h \
    modelos/ordenes.h \
    modelos/producto.h \
    modelos/productos.h \
    modelos/rest.hpp \
    modelos/direcciones.h \
    modelos/pregunta.h \
    modelos/preguntas.h \
    modelos/valor.h \
    modelos/valores.h \
    modelos/pedido.h \
    modelos/pedidos.h

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
    ANDROID_EXTRA_LIBS =
}

DESTDIR = $$PWD
